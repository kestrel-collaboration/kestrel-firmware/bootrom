////////////////////////////////////////////////////////////////////////////////
///
/// @file       stack_checker.c
///
/// @project
///
/// @brief      Routines to check the stack usage..
///
////////////////////////////////////////////////////////////////////////////////
///
////////////////////////////////////////////////////////////////////////////////
///
/// @copyright Copyright (c) 2021, Evan Lojewski
/// @cond
///
/// All rights reserved.
///
/// Redistribution and use in source and binary forms, with or without
/// modification, are permitted provided that the following conditions are met:
/// 1. Redistributions of source code must retain the above copyright notice,
/// this list of conditions and the following disclaimer.
/// 2. Redistributions in binary form must reproduce the above copyright notice,
/// this list of conditions and the following disclaimer in the documentation
/// and/or other materials provided with the distribution.
/// 3. Neither the name of the copyright holder nor the
/// names of its contributors may be used to endorse or promote products
/// derived from this software without specific prior written permission.
///
////////////////////////////////////////////////////////////////////////////////
///
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
/// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
/// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
/// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
/// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
/// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
/// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
/// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
/// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
/// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
/// POSSIBILITY OF SUCH DAMAGE.
/// @endcond
////////////////////////////////////////////////////////////////////////////////

#include <stack_checker.h>
#include <stdio.h>
#include <string.h>

#define STACK_SENTINEL (0xC051AC0Cu)

static struct
{
    void *low_stack;
    void *high_stack;

    void *first_free;
} stack_checker_state;

void stack_checker_init(void *_fstack, void *_estack)
{
    stack_checker_state.low_stack = _fstack;
    stack_checker_state.high_stack = _estack;

    // Set all values in the stack to a known sentinel
    for (void *pos = _fstack; pos < _estack; pos += sizeof(uint32_t))
    {
        *(uint32_t *)pos = STACK_SENTINEL;
    }

    // The stack grows from a high address towards a low address.
    // Assume no stack space is used by default.
    stack_checker_state.first_free = stack_checker_state.high_stack;
}

int32_t stack_checker_size(void)
{
    return stack_checker_state.high_stack - stack_checker_state.low_stack;
}

int32_t stack_checker_check(void)
{
    for (void *check_pos = stack_checker_state.low_stack; check_pos < stack_checker_state.first_free; check_pos += sizeof(uint32_t))
    {
        if (*(uint32_t *)check_pos != STACK_SENTINEL)
        {
            stack_checker_state.first_free = check_pos;
            // Unused location found. Break;
            break;
        }
    }

    return stack_checker_state.first_free - stack_checker_state.low_stack;
}
