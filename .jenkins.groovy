////////////////////////////////////////////////////////////////////////////////
///
/// @file       .jenkins.groovy
///
/// @project
///
/// @brief      Main Jenkins configuration
///
////////////////////////////////////////////////////////////////////////////////
///
////////////////////////////////////////////////////////////////////////////////
///
/// @copyright Copyright (c) 2019-2021, Evan Lojewski
/// @cond
///
/// All rights reserved.
///
/// Redistribution and use in source and binary forms, with or without
/// modification, are permitted provided that the following conditions are met:
/// 1. Redistributions of source code must retain the above copyright notice,
/// this list of conditions and the following disclaimer.
/// 2. Redistributions in binary form must reproduce the above copyright notice,
/// this list of conditions and the following disclaimer in the documentation
/// and/or other materials provided with the distribution.
/// 3. Neither the name of the copyright holder nor the
/// names of its contributors may be used to endorse or promote products
/// derived from this software without specific prior written permission.
///
////////////////////////////////////////////////////////////////////////////////
///
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
/// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
/// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
/// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
/// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
/// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
/// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
/// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
/// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
/// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
/// POSSIBILITY OF SUCH DAMAGE.
/// @endcond
////////////////////////////////////////////////////////////////////////////////

def notify(status, description)
{
    updateGitlabCommitStatus name: 'build', state: status
}

def build(nodeName, archive = false, archive_cab = false, analyze = true, test_archive = false)
{
    node(nodeName)
    {
        cleanWs()
        def URL = ''
        def REFSPEC = env['gitlabBranch']
        if (env['gitlabTargetRepoHttpUrl'])
        {
            URL = env['gitlabTargetRepoHttpUrl']
        }
        else
        {
            if (env['gitlabSourceRepoHttpUrl'])
            {
                URL = env['gitlabSourceRepoHttpUrl']
            }
            else
            {
                URL = 'http://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-firmware/bootrom.git'
            }
        }
        def HASH = ''
        if (env['gitlabMergeRequestIid'])
        {
            // Merge request
            HASH = 'merge-requests/' + env['gitlabMergeRequestIid']
            REFSPEC = '+refs/heads/*:refs/remotes/origin/* +refs/merge-requests/*/head:refs/remotes/origin/merge-requests/*'
        }
        else
        {
            // Push to repo
            HASH = 'origin/' + env['gitlabSourceBranch}']
            REFSPEC = env['gitlabBranch']
        }

        stage('checkout')
            {
            def scmVars = checkout(
                [$class: 'GitSCM',
                    branches: [[name: HASH]],
                    userRemoteConfigs: [[
                        refspec: REFSPEC,
                        url: URL
                    ]],
                    browser: [$class: 'GitLab', repoUrl: 'http://gitlab.raptorengineering.com/kestrel-collaboration/kestrel-firmware/bootrom'],
                    doGenerateSubmoduleConfigurations: false,
                    extensions: [
                        [$class: 'SubmoduleOption',
                            disableSubmodules: false,
                            parentCredentials: false,
                            recursiveSubmodules: false,
                            reference: '',
                            trackingSubmodules: false
                        ],
                    ],
                    submoduleCfg: []
                ])

            def GIT_SUBJECT = sh (
                script: 'git show -s --format=%s',
                returnStdout: true,
            ).trim()
            currentBuild.description = GIT_SUBJECT
        }

        stage('build')
        {
            if (analyze)
            {
                sh './build.sh'
            }
            else
            {
                sh './build.sh -DDISABLE_CLANG_ANALYZER=True'
            }

            if (archive)
            {
                dir('build')
                {
                    archiveArtifacts artifacts: '*.zip', fingerprint: true
                    archiveArtifacts artifacts: '*.tar.gz', fingerprint: true
                }
            }
        }

        cleanWs()
    }
}

try
{
    notify('pending', 'Build Pending ')
    parallel(
        "fedora": { build('master', true, true, true, true) },
        // "ubuntu-18.04": { build('ubuntu-18.04', false, false, false, false) },
        // "ubuntu-20.04": { build('ubuntu-20.04', true, false, false, false) },
        // "freebsd-12": { build('freebsd-12', true, false, false, false) },
    )
}
catch(e)
{
    currentBuild.result = 'FAILURE'
    throw e
}
finally
{
    cleanWs()

    def currentResult = currentBuild.result ?: 'SUCCESS'
    if(currentResult == 'SUCCESS')
    {
        notify('success', 'Build Passed ')
    }
    else
    {
        notify('failed', 'Build Failed ')
    }
}
