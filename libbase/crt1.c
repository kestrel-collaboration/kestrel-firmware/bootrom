////////////////////////////////////////////////////////////////////////////////
///
/// @file       crt1.c
///
/// @project
///
/// @brief      Support routines to bring up the system before calling main.
///
////////////////////////////////////////////////////////////////////////////////
///
////////////////////////////////////////////////////////////////////////////////
///
/// @copyright Copyright (c) 2021, Evan Lojewski
/// @cond
///
/// All rights reserved.
///
/// Redistribution and use in source and binary forms, with or without
/// modification, are permitted provided that the following conditions are met:
/// 1. Redistributions of source code must retain the above copyright notice,
/// this list of conditions and the following disclaimer.
/// 2. Redistributions in binary form must reproduce the above copyright notice,
/// this list of conditions and the following disclaimer in the documentation
/// and/or other materials provided with the distribution.
/// 3. Neither the name of the copyright holder nor the
/// names of its contributors may be used to endorse or promote products
/// derived from this software without specific prior written permission.
///
////////////////////////////////////////////////////////////////////////////////
///
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
/// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
/// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
/// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
/// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
/// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
/// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
/// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
/// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
/// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
/// POSSIBILITY OF SUCH DAMAGE.
/// @endcond
////////////////////////////////////////////////////////////////////////////////

#include <irq.h>
#include <stack_checker.h>
#include <string.h>

extern char _fbss[];
extern char _ebss[];

extern char _fdata_rom[] __attribute((weak));
extern char _fdata[];
extern char _edata[];

extern char _fstack[];
extern char _estack[];

int main(int i, char **c);
void crt1(void);

// Default exception handler
extern void _exception_entry(void);
extern char _exception_entry_size[];
extern char _ivt_base[];

//lint -esym(714, crt1) // Referenced by crt0.s
void __attribute__((noreturn)) crt1(void)
{
    // This is the main c entry point. Note that no functions that depend
    // on initialized data should be called until after the data and bss
    // sections are initialized.

    // Copy Data from ROM to RAM if needed.
    if (_fdata_rom) //lint !e506 !e774
    {
        memcpy(_fdata, _fdata_rom, (size_t)(_edata - _fdata));
    }

    // Initialize BSS
    memset(_fbss, 0, (size_t)(_ebss - _fbss));

    // Initialize stack checker to check for overflows and max usage.
    stack_checker_init(_fstack, _estack);

    // Initialize the IVT
    // clang-format off
    static const uint16_t vectors[] = {
        EXCEPTION_DSI,
        EXCEPTION_DSegI,
        EXCEPTION_ISI,
        EXCEPTION_ISegI,
        EXCEPTION_EXTERNAL_IRQ,
        EXCEPTION_PROGRAM,
        EXCEPTION_DECREMENTER,
        EXCEPTION_SYSTEM_CALL,
    };
    // clang-format on

    irq_set_base(_ivt_base);
    for (size_t i = 0; i < sizeof(vectors) / sizeof(vectors[0]); i++)
    {
        memcpy(_ivt_base + vectors[i], _exception_entry, (size_t)_exception_entry_size);
    }

    // Jump to the main routine
    (void)main(0, NULL);
    for (;;)
    {
        // Spin.
    }
}
