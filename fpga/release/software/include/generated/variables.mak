ARCHITECTURE=powerpc64le
TRIPLE=powerpc64le-linux-gnu
CPU=microwatt
CPUFLAGS=-m64 -mabi=elfv2 -msoft-float -mno-string -mno-multiple -mno-vsx -mno-altivec -mlittle-endian -mstrict-align -fno-stack-protector -mcmodel=small -D__microwatt__ 
CPUENDIANNESS=little
CLANG=0
CPU_DIRECTORY=/disk2/KESTREL/litex-repos/litex.PUBLIC/litex/soc/cores/cpu/microwatt
SOC_DIRECTORY=/disk2/KESTREL/litex-repos/litex.PUBLIC/litex/soc
COMPILER_RT_DIRECTORY=/home/video-editor/.local/lib/python3.11/site-packages/pythondata_software_compiler_rt-0.0.post6206-py3.11.egg/pythondata_software_compiler_rt/data
export BUILDINC_DIRECTORY
BUILDINC_DIRECTORY=/disk2/KESTREL/litex-repos/litex-boards.PUBLIC/litex-boards/litex_boards/targets/build/rcs_arctic_tern_bmc_card/software/include
LIBCOMPILER_RT_DIRECTORY=/disk2/KESTREL/litex-repos/litex.PUBLIC/litex/soc/software/libcompiler_rt
LIBBASE_DIRECTORY=/disk2/KESTREL/litex-repos/litex.PUBLIC/litex/soc/software/libbase
LIBFATFS_DIRECTORY=/disk2/KESTREL/litex-repos/litex.PUBLIC/litex/soc/software/libfatfs
LIBLITESPI_DIRECTORY=/disk2/KESTREL/litex-repos/litex.PUBLIC/litex/soc/software/liblitespi
LIBLITEDRAM_DIRECTORY=/disk2/KESTREL/litex-repos/litex.PUBLIC/litex/soc/software/liblitedram
LIBLITEETH_DIRECTORY=/disk2/KESTREL/litex-repos/litex.PUBLIC/litex/soc/software/libliteeth
LIBLITESDCARD_DIRECTORY=/disk2/KESTREL/litex-repos/litex.PUBLIC/litex/soc/software/liblitesdcard
LIBLITESATA_DIRECTORY=/disk2/KESTREL/litex-repos/litex.PUBLIC/litex/soc/software/liblitesata
BIOS_DIRECTORY=/disk2/KESTREL/litex-repos/litex.PUBLIC/litex/soc/software/bios